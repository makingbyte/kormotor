@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Busqueda - Modelo</h2>
        </div>
        <div class="col-md-12">

              @php

                foreach (json_decode($dd) as $key) {
               @endphp    
                   <div class="col-md-12"><a href="{{ url ('') }}/busqueda/{{ $key->label }}"> {{$key->label}} ( {{$key->value}} )</div> 
                   @php
                }

              @endphp

        </div>
        </div>
    </div>
    @endauth
</div>
@endsection
