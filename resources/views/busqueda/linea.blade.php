@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Busqueda - Linea</h2>
        </div>
        <div class="col-md-12">

              @php

                foreach ($data as $key) {
               @endphp    
                   <div class="col-md-12"><a href="{{ url ('') }}/busqueda/{{ $key->categoria }}"> {{$key->categoria}} ( {{$key->cuantos}} )</div> 
                   @php
                }

              @endphp

        </div>
        </div>
    </div>
    @endauth
</div>
@endsection
