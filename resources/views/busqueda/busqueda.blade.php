@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Busqueda</h2>
        </div>
        <div class="col-md-12 float-left">
          <form method="POST" action="{{ url ('') }}/busqueda">
                @csrf
              <div class="form-group">
                <input type="text" class="form-control" id="busq" name="busq" aria-describedby="busq" placeholder="Escribe lo que buscas">
              </div>
            </form>
        </div>
        <div class="col-md-12 float-left">

            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Marca</th>
                  <th scope="col">Modelo</th>
                  <th scope="col">Producto</th>
                  <th scope="col">U/M</th>
                  <th scope="col">Procedencia</th>
                  <th scope="col">Fabricante</th>                  
                  <th scope="col">Categoria</th>
                  <th scope="col">Costo</th>                  
                  <th scope="col">Oferta</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->marca }}</td>
                  <td>
                    @foreach(explode('|', $key->iden_modelo ) as $info) 
                      {{$info}}<br>
                    @endforeach
                  </td>                
                  <td><a href="{{ url ('') }}/articulo/{{ $key->idart}}">{{ $key->desc_articulo }}</a></td>
                  <td>{{ $key->desc_umedida }}</td>
                  <td>{{ $key->procedencia }}</td>
                  <td>{{ $key->fabricante }}</td> 
                  <td>{{ $key->categoria }}</td>
                  @php
                  if(!empty($key->mnto_oferta)){
                  @endphp                  
                  <td class="text-secondary" style="text-decoration:line-through;">{{ number_format($key->cant_costo) }}</td> 
                  @php
                  }else{
                  @endphp                  
                  <td class="text-secondary">{{ number_format($key->cant_costo) }}</td>
                  @php
                  }
                  @endphp 
                  <td>{{number_format(($key->cant_costo/100 * (100-$key->mnto_oferta))) }}</td> 
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div>
    </div>
    @endauth
</div>
@endsection
