@extends('layouts.app')

@section('content')

<script src="{{ asset('js/carro.js') }}" defer></script>

<?php
//vars
$montoFinal = 0;
?>
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Carro</h2>
        </div>
        @csrf
        @foreach($data as $key)
            <div class="col-12 border-top border-bottom p-1" style="height: 80px;">
                <div class="col-12 float-left">
                    <div class="col-1 float-left">
                        <img class="rounded mx-auto d-block p-1" height="60" src="/kormotor/kormotor/public/img/articulo/{{$key->codi_articulo}}.jpg">
                    </div>
                    <div class="col-4 float-left" style="font-size: 10px;">
                       <div class="col-12 text-primary">{{$key->codi_articulo}}</div>
                       <div class="col-12 text-info">{{$key->desc_articulo}}</div>
                       <div class="col-12">{{$key->iden_modelo}}</div>
                    </div>
                    <div class="col-2 float-left">
                        <div class="col-12">
                            <select class="border p-1 m-1"  style="font-size: 12px; width: 90%">
                                <option selected>Ver Aplicaciones</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <select class="border p-1 m-1"  style="font-size: 12px; width: 90%">
                                <option selected>Numnero de partes orginales</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-1 float-left  pt-2">
                         <select class="custom-select my-1 mr-sm-2 cantidad" id="cantidad-{{$key->idcar}}">
                            <option>Cantidad</option>                    
                            <option value="0" <?=($key->mont_cantidad == 0 ) ? 'selected' : ''?> >0</option>
                            @php
                                for($i=1;$i<=$key->nmro_stock;$i++ ){
                            @endphp
                                <option value="{{$i}}" <?=($key->mont_cantidad == $i ) ? 'selected' : ''?> >{{$i}}</option>
                            @php
                                }
                            @endphp
                            <!--option value="2" <?=($key->mont_cantidad == 2 ) ? 'selected' : ''?> >2</option>
                            <option value="3" <?=($key->mont_cantidad == 3 ) ? 'selected' : ''?> >3</option-->
                         </select>
                    </div>
                    <div class="col-2 float-left  pt-4 text-right">
                         $ {{$key->cant_costo}}
                         <input type="hidden" class="montoProducto" value="{{$key->cant_costo}}" name="monto" id="{{$key->idcar}}">
                    </div>
                    <div class="col-2 float-left  pt-4 text-right" id="total-1">
                         $ {{$key->cant_costo}}
                    </div>
                </div>
                <!--div class="col-12 float-left border botder-primary">
                    <div class="col-3 float-left">Borrar</div>
                </div-->
            </div>
            <?php
                $montoFinal += ($key->mont_cantidad * $key->cant_costo);
            ?>
        @endforeach

        <div class="col-12 mt-2">
            <div class="col-6 float-left"></div>
            <div class="col-6 float-left">
                <div class="col-12">Total neto: <spawn class="totalcomun text-right">$ <?=$montoFinal?></spawn></div>
                <div class="col-12">Iva: <spawn class="totaliva text-right">$ <?=ceil(($montoFinal/100)*19)?></spawn></div>
                <div class="col-12">Neto + Iva:  <spawn class="totaltotal text-right">$ <?=ceil($montoFinal+($montoFinal/100)*19)?></spawn></div>
            </div>
        </div>
    </div>
    @endauth
</div>
@endsection

