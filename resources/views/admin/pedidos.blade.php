@extends('layouts.dashboard')
@section('page_heading','Pedidos')
@section('section')
           
@auth
    <div class="col-md-8 float-left">
    	 <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Cliente</th>
                  <th scope="col">Producto</th>                  
                  <th scope="col"></th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->name }}</td>
                  <td></td>                
                  <td><a href="{{url('pedidos/')}}/{{$key->id}}">Ver</a></td> 
                </tr>
                @endforeach                
              </tbody>
            </table>
    </div>
    @endauth          
            
@stop
