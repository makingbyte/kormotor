@extends('layouts.dashboard')
@section('page_heading','Banner')
@section('section')
           
@auth
    <div class="col-md-8 float-left">
    	<div class="col-12">
    		<img src="{{ url ('') }}/img/logo.png">
    	</div>
        <form enctype="multipart/form-data" class="form-group" method="post" enctype="multipart/form-data" action="{{ url ('') }}/banner" >
             
			 @csrf
			<div class="form-group">
				<label>imagen</label>
				<input type="file" name="imagen"/>
			</div>
			<button class="btn btn-primary">Actualizar</button>
		</form>
    </div>
    @endauth          
            
@stop
