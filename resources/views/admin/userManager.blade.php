@extends('layouts.dashboard')
@section('page_heading','Crear Usuario')
@section('section')
@include('common.alert')
    @auth
    <div class="col-md-9 float-left">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Email</th>
                  <th scope="col">Cuenta Corriente</th>
                  <th scope="col">Descuento</th>
                  <th scope="col">Accion</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <th scope="row">{{ $key->id }}</th>                 
                  <td>{{ $key->name }}</td>
                  <td>{{ $key->email }}</td>
                  <td>{{ $key->cuenta_corriente }}</td>
                  <td>{{ $key->descuento }}</td>
                  <td><a href="{{ url ('') }}/createUser/{{ $key->id }}/edit" class="text-success mr-5">Editar @include('widgets.icon', array('class'=>'pencil')) </a> | @include('widgets.icon', array('class'=>'trash-o'))</td>
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div> 
    @endauth

@endsection