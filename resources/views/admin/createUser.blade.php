@extends('layouts.dashboard')
@section('page_heading','Crear Usuario')
@section('section')
@include('common.alert')
    @auth
        <div class="col-md-8 float-left">
        <form enctype="multipart/form-data" class="form-group" method="post" enctype="multipart/form-data" action="{{ url ('') }}/createUser" >
			@csrf

			<div class="form-group">
				<label>Nombre</label>
				<input type="text" class="form-control" name="name"/>
			</div>

            <div class="form-group">
				<label>Correo</label>
				<input type="email" class="form-control" name="email"/>
			</div>

            <div class="form-group">
				<label>Password</label>
				<input type="password" class="form-control" name="password"/>
			</div>

            <div class="form-group">
				<label>Cuenta Corriente</label>
				<input type="text" class="form-control" name="cuenta"/>
			</div>

			<div class="form-group">
				<label>Descuento</label>
				<input type="text" class="form-control" name="descuento"/>
			</div>

			<button class="btn btn-primary">Crear</button>
		</form>
            
            <div class="card-columns">

          
        </div>
    </div>
    @endauth

@endsection