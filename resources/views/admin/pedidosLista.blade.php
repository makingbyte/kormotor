@extends('layouts.dashboard')
@section('page_heading','Pedidos')
@section('section')
           
@auth
    <div class="col-md-11 float-left">
    	 <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Producto</th>
                  <th scope="col">Modelo</th>                  
                  <th scope="col">Codigo</th>                 
                  <th scope="col">Fabricante</th>                 
                  <th scope="col">Procedencia</th>               
                  <th scope="col">Cantidad</th>                                 
                  <th scope="col">Costo</th>
                  <th scope="col">Total</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->desc_articulo }}</td>
                  <td>{{ $key->modelo }}</td>
                  <td>{{ $key->codi_articulo }}</td>                
                  <td>{{ $key->fabricante }}</td>               
                  <td>{{ $key->procedencia }}</td>
                  <td>{{ $key->mont_cantidad }}</td>            
                  <td>{{ $key->cant_costo }}</td>
                  <td>{{ $key->cant_costo * $key->mont_cantidad }} </td>
                  @php
                  @$totalCal += $key->cant_costo * $key->mont_cantidad;
                  @endphp 
                </tr>
                @endforeach 
                 <tr style="font-size: 12px;">
                  <td></td>
                  <td></td>
                  <td></td>                
                  <td></td>               
                  <td></td>
                  <td></td>            
                  <td>SubTotal</td>
                  <td>{{$totalCal}}</td> 
                </tr>    

                 <tr style="font-size: 12px;">
                  <td></td>
                  <td></td>
                  <td></td>                
                  <td></td>               
                  <td></td>
                  <td></td>            
                  <td>iva</td>
                  <td>{{ceil($totalCal/100*19)}}</td> 
                </tr>


                 <tr style="font-size: 12px;">
                  <td></td>
                  <td></td>
                  <td></td>                
                  <td></td>               
                  <td></td>
                  <td></td>            
                  <td>Total</td>
                  <td>{{ceil($totalCal+($totalCal/100*19))}}</td> 
                </tr>                
              </tbody>
            </table>
    </div>
    @endauth          
            
@stop
