@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Ofertas</h2>
        </div>
        <div class="col-3 float-left">
            <nav id="navbar-example3" class="navbar navbar-light bg-light">                
              <nav class="nav nav-pills flex-column">
                <a class="navbar-brand" href="#">Categorias</a>  

                @foreach($ofertaFiltro as $key)
                    <a class="nav-link" href="ofertas/list/{{$key->iden_marca}}">
                    {{ $key->desc_marca }}({{ $key->cuantos}})
                    </a>
                @endforeach                
              </nav>
            </nav>
        </div>
 
        <div class="col-md-9 float-left">

            <div class="card-columns">

            @foreach($data as $key)
              <div class="card">
                <a class="nav-link" href="ofertas/{{$key->iden_oferta}}">
                    {{ $key->desc_titulo }}
                    <img class="card-img" src="/kormotor/kormotor/public/img/oferta/{{ $key->desc_imagen }}" alt="Card image">
                </a>
              </div>
            @endforeach         


            </div>
                <!--div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div-->

        </div>
    </div>
    @endauth
</div>
@endsection
