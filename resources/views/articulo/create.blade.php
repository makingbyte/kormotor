@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Ofertas</h2>
        </div>
 
        <div class="col-md-8 float-left">
            <form method="post" action="{{url('import-articulo')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="file" name="excel">
                <br><br>
                <input type="submit" value="Enviar" style="padding: 10px 20px;">
            </form>

        </div>       
            
    </div>
    @endauth
</div>
@endsection
