@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <div class="card p-2 pb-5">
    <div class="row justify-content-center">
        <div class="col-10 p-2 mb-6  border-bottom border-primary">
            <div class="col-md-4 float-left">{{$data->desc_articulo}}</div>
            <div class="col-md-4 float-left">Codigo del producto: {{$data->codi_articulo}} </div>

        </div>        
        <div class="col-md-12 float-left mt-4">
          <div class="col-md-12 float-left">
            <div class="col-md-3 float-left">
                <img class="rounded mx-auto d-block p-0 border" height="200" src="/kormotor/kormotor/public/img/articulo/{{$data->codi_articulo}}.jpg">              
            </div>            
            <div class="col-md-9 float-left">

              <div class="col-md-12 float-left mb-9">
                <div class="col-md-12 float-left">
                <!-- info aqui -->
                </div>
                <div class="col-md-12 float-left">                  
                  <div class="col-md-12">Precio Neto</div>
                  <div class="col-md-12 text-danger">$ {{$data->cant_costo}}</div>
                </div>
              </div>
              <div class="col-md-12 float-left border-bottom border-primary">
                Especicaciones
              </div> 
              <div class="col-md-12" style="font-size: 10px;">
                <div class="col-md-4">Marca: {{$data->marca}}</div>                
                <div class="col-md-4">Marca: {{$data->modelo}}</div>
                <div class="col-md-4">Origen: {{$data->procedencia}}</div>                
                <div class="col-md-4">Categoria: {{$data->categoria}}</div>                
                <div class="col-md-4">Stock:  {{$data->nmro_stock}}</div>
              </div>
              <div class="col-md-4 float-right">
              <form enctype="multipart/form-data" class="form-group" method="post" enctype="multipart/form-data" action="/kormotor/kormotor/public/carro">
                @csrf
                <input type="hidden" name="iden_articulo" value="{{$data->id}}">
                <input type="hidden" name="mont_cantidad" value="1">
                <button type="submit"class="text-center btn btn-primary">Agregar al carrito</button>
              </form>
            
            </div>
            </div>
          </div>  
        </div>
    </div>
    </div>
    @endauth
</div>
@endsection
