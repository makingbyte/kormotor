
@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Busqueda</h2>
        </div>
 
        <div class="col-md-12 float-left">

            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Marca</th>
                  <th scope="col">Modelo</th>
                  <th scope="col">Producto</th>
                  <th scope="col">Año I</th>
                  <th scope="col">Origen</th>
                  <th scope="col">Costo</th>                  
                  <th scope="col">Oferta</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->desc_marca }}</td>
                  <td>
                    @foreach(explode('|', $key->iden_modelo ) as $info) 
                      {{$info}}<br>
                    @endforeach
                  </td>                
                  <td><a href="{{ url ('') }}/articulo/{{ $key->id}}">{{ $key->desc_articulo }}</a></td>
                  <td>{{ $key->fech_anioi }}</td>
                  <td>{{ $key->procedencia }}</td>                  
                  <td class="text-secondary" style="text-decoration:line-through;">{{ number_format($key->cant_costo, 2) }}</td> 
                  <td>{{number_format(($key->cant_costo/100 * (100-$key->mnto_oferta)), 2) }}</td> 
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div>
    </div>
    @endauth
</div>
@endsection
