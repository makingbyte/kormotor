@extends('layouts.dashboard')
@section('page_heading','Crear Ofertas')
@section('section')

    @auth
        <div class="col-md-8 float-left">
        <form enctype="multipart/form-data" class="form-group" method="post" enctype="multipart/form-data" action="/ofertas/{{$data->iden_oferta}}" >
			@method('PUT')
			@csrf

			<div class="form-group">
				<label>Nombre</label>
				<input type="text" class="form-control" name="name" value="{{$data->desc_oferta}}"/>
			</div>

			<div class="form-group">
				<label>estado</label>
                <select class="form-control" id="exampleFormControlSelect1" name="estado">
                    <option value="1">activo</option>
                    <option value="2">inactivo</option>
                </select>
			</div>

			<div class="form-group">
				<label>imagen</label>
				<input type="file" name="imagen"/>
			</div>

			<button class="btn btn-primary">Guardar</button>
		</form>
            
            <div class="card-columns">

          
        </div>
    </div>
    @endauth

@endsection
