@extends('layouts.dashboard')
@section('page_heading','Marcas')
@section('section')
	<div class="col-md-9 float-left">
            <table class="table table-striped">
              <thead>
                <tr>

                  <th scope="col">Descripcion</th>
                  <th scope="col">Monto</th>
                  <th scope="col">Accion</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->desc_oferta }}</td>
                  <td>{{ $key->mnto_oferta }}</td>              
                  <td><a href="ofertas/{{ $key->iden_oferta }}/edit" class="text-success mr-5">Editar @include('widgets.icon', array('class'=>'pencil')) </a> | @include('widgets.icon', array('class'=>'trash-o'))</td>
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div>               
   @endsection     
