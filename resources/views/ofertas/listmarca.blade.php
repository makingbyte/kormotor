@extends('layouts.app')

@section('content')
<div class="container">
    @auth
    <?php
        $origen[1] = 'Taiwan';
        $origen[2] = 'America';
        $origen[3] = 'Corea';
    ?>
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Ofertas</h2>
        </div>
        <div class="col-3 float-left">
            <nav id="navbar-example3" class="navbar navbar-light bg-light">                
              <nav class="nav nav-pills flex-column">
                <a class="navbar-brand" href="#">Categorias</a>  

                @foreach($ofertaFiltro as $key)
                
                    <a class="nav-link" href="{{$key->marca}}">
                    {{ $key->marca }}({{ $key->cuantos}})
                    </a>
                @endforeach                
              </nav>
            </nav>
        </div>
 
        <div class="col-md-9 float-left">

            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Marca</th>
                  <th scope="col">Modelo</th>
                  <th scope="col">Producto</th>
                  <th scope="col">U/M</th>
                  <th scope="col">Procedencia</th>
                  <th scope="col">Fabricante</th>                  
                  <th scope="col">Categoria</th>
                  <th scope="col">Costo</th>                  
                  <th scope="col">Oferta</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($articuloOferta as $key)
                <tr style="font-size: 12px;">
                  <td>{{ $key->marca }}</td>
                  <td>
                    @foreach(explode('|', $key->iden_modelo ) as $info) 
                      {{$info}}<br>
                    @endforeach
                  </td>                
                  <td><a href="{{ url ('') }}/articulo/{{ $key->id}}">{{ $key->desc_articulo }}</a></td>
                  <td>{{ $key->desc_umedida }}</td>
                  <td>{{ $key->procedencia }}</td>
                  <td>{{ $key->fabricante }}</td> 
                  <td>{{ $key->categoria }}</td>                  
                  <td class="text-secondary" style="text-decoration:line-through;">{{ number_format($key->cant_costo) }}</td> 
                  <td>{{number_format(($key->cant_costo/100 * (100-$key->mnto_oferta))) }}</td> 
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div>
    </div>
    @endauth
</div>
@endsection
