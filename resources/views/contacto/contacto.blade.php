@extends('layouts.app')

@section('content')

<script src="{{ asset('js/carro.js') }}" defer></script>

<?php
//vars
$montoFinal = 0;
?>
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-6">
            <h2>Contacto</h2>
        </div>
        <div class="col-12">
            <div class="col-8 float-left p-0">
                <img src="{{ url ('/img/back/contacto.jpg') }}" width="100%" alt="">
            </div>
            <div class="col-4 bg bg-primary float-left h-100 ml-0 text-white">
                <div class="col-12 text-left border-bottom mt-3">
                    <h5><i class="fas fa-phone-square"></i> Telefono: +56 9 2657 4593</h5>
                </div>
                <div class="col-12 text-left border-bottom mt-3">
                   <h5><i class="fas fa-envelope-square"></i> Correo: kormotor@kotmotor.cl </h5>
                </div>
                <div class="col-12 text-left border-bottom mt-3">
                   <h5><i class="far fa-clock"></i> Horario: 9:30am - 6:00pm  </h5>
                </div>
            </div>
        </div> 
        <div class="col-12 mt-2 pl-10">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3328.8038822763224!2d-70.64696468531615!3d-33.45441568077294!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662c572c5fab86d%3A0x5d9b86b82c7f27f9!2sCopiap%C3%B3+733%2C+Santiago%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1ses!2scl!4v1546223401764" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>           
    </div>
    @endauth
</div>
@endsection

