<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Koromotor - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                @if (Auth::check() == 1)
                <a class="navbar-brand border-bottom border-danger" href="{{ url('/home') }}">
                    Buscador
                </a>
                <a class="navbar-brand " href="{{ url('/buscarmodelo') }}">
                    Modelo
                </a>
                <a class="navbar-brand" href="{{ url('/buscarlinea') }}">
                    Linea
                </a>
                <a class="navbar-brand" href="{{ url('/home') }}">
                    OEM-Faábrica
                </a>
                <a class="navbar-brand" href="{{ url('/home') }}">
                    Marca
                </a>                
                <a class="navbar-brand" href="{{ url('/home') }}">
                    Medida
                </a>
                @endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <!--a class="nav-link" href="{{ route('register') }}">{{ __('Registrar') }}</a-->
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ url('/') }}/admin">
                                       Panel
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if (Auth::check())
        <ul class="nav justify-content-end pl-2">
          <li class="nav-item">
            <a class="nav-link active" href="{{ url('/carro') }}">Carro</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/ofertas') }}">Ofertas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ url('/contacto') }}">Contacto</a>
          </li>
        </ul>
        @endif
        <!--div class="row border border-warning">
            <div class="container border border-success">      
                <div class="col-12 text-right">Carro  Ofertas  Contacto</div>                
            </div>
        </div-->
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
