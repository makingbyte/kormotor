@extends('layouts.dashboard')
@section('page_heading','Modelo')
@section('section')
	<div class="col-md-9 float-left">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Marca</th>
                  <th scope="col">Modelo</th>
                  <th scope="col">Accion</th>
                </tr>
              </thead>                
              <tbody>

               @foreach($data as $key)
                <tr style="font-size: 12px;">
                  <th scope="row">{{ $key->id }}</th>                 
                  <td>{{ $key->desc_marca }}</td>
                  <td>{{ $key->desc_modelo }}</td>
                  <td><a href="marca/{{ $key->id }}/edit" class="text-success mr-5">Editar @include('widgets.icon', array('class'=>'pencil')) </a> | @include('widgets.icon', array('class'=>'trash-o'))</td>
                </tr>
                @endforeach                
              </tbody>
            </table>

        </div>               
   @endsection         

