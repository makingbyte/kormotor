@extends('layouts.app')

@section('title', 'ofertas')

@section('content')
<div class="container">
    @auth
    <div class="row justify-content-center">
        <div class="col-12 mb-0 justify-content-center" style="margin: 0 auto;" id="logo-bus">
            <img class="rounded mx-auto d-block" src="{{ url ('') }}/img/logo.png">
        </div>
        <div class="col-md-8">
             
            <form method="POST" action="busqueda">
                @csrf
              <div class="form-group">
                <input type="text" class="form-control" id="busq" name="busq" aria-describedby="busq" placeholder="Escribe lo que buscas">
              </div>
            </form>


            <div class="card-columns">
              
              @foreach($data as $key)
              <div class="card">
                <a class="nav-link" href="ofertas/{{$key->iden_oferta}}">
                    {{ $key->desc_titulo }}
                    <img class="card-img" src="/kormotor/kormotor/public/img/oferta/{{ $key->desc_imagen }}" alt="Card image">
                </a>
              </div>
            @endforeach
              


            </div>
                <!--div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div-->

        </div>
    </div>
    @endauth
</div>
@endsection
