@extends('layout.app')

@section('title', 'home')

@section('content')

		<div class="row">
			<div class="col-6 mx-auto p-3 bg-light row">
				<div class="mx-auto col-6"><img src="/img/logo.png"></div>
				<div class="mx-auto col-10">
					<form>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Email</label>
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ingrese">
				    <small id="emailHelp" class="form-text text-muted">Nunca compartiremos su correo electrónico con nadie más.</small>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Clave</label>
				    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Clave">
				  </div>
				  
				  <button type="submit" class="btn btn-primary float-right">Iniciar</button>
				</form>
				</div>
				
			</div>
		</div>			
	

@endsection