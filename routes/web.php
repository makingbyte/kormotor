<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('home', function (){
	return view('home');
});

Route::get('/mi_primera_ruta', function(){
	return view('home');
});


/*Route::get('/ofertas', function(){
	return view('ofertas.ofertas');
});*/

Route::resource('articulo', 'articuloController');
Route::resource('ofertas', 'ofertaController');
Route::resource('carro', 'carroController');
Route::resource('contacto', 'contactoController');
Route::resource('marca', 'marcaController');
Route::resource('modelo', 'modeloController');
Route::resource('busqueda', 'busquedaController');
Route::resource('admin', 'adminController');
Route::resource('createUser', 'userManagerController');
Route::resource('banner', 'bannerController');
Route::resource('pedidos', 'pedidosController');

Route::get('ofertas/list/{id}', 'ofertaController@list')->name('Ofertas');
Route::get('ofertas/listmarca/{id}', 'ofertaController@listmarca')->name('ofertaslista');

//Busqueda
//Route::get('articulo/busqueda/{param}', 'articuloController@busqueda')->name('Ofertas');
Route::post('busqueda', 'busquedaController@index')->name('busqueda');
//Route::resource('buscarmodelo', 'busquedaController@modelo');
Route::get('/buscarmodelo', 'busquedaController@modelo');
Route::get('/buscarlinea', 'busquedaController@linea');
Route::get('/adminOferta', 'ofertaController@lista');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/export-users', 'ExcelController@exportUsers');
Route::post('/import-articulo', 'articuloController@import');