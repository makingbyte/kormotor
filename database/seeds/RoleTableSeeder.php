<?php

use Illuminate\Database\Seeder;
use App\Role;



class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "admin";
        $role->description = "Usuario con todos los provilegios";
        $role->save();

        $role = new Role();
        $role->name = "user";
        $role->description = "Usuario con privilegios basicos";
        $role->save();
    }
}
