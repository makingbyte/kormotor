<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('desc_articulo');
            $table->integer('iden_marca');
            $table->integer('iden_modelo');
            $table->integer('iden_procedencia');
            $table->integer('iden_fabricante');
            $table->integer('iden_oem');
            $table->integer('nmro_stock');
            $table->decimal('cant_costo',10,2);
            $table->integer('iden_oferta');            
            $table->integer('fech_anioi');            
            $table->integer('fech_aniot');
            $table->integer('desc_umedida');                        
            $table->integer('codi_articulo');
            $table->integer('iden_familia');
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
