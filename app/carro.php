<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class carro extends Model
{
    //
    public static function SetCarro($var){

    	$carro = new carro;
    	$carro->iden_user 	  = Auth::user()->id;
    	$carro->iden_articulo = $var->iden_articulo;
    	$carro->mont_cantidad = $var->mont_cantidad;
    	$carro->iden_estado   = 0;
    	$carro->save();

    }

    public static function GetCarro(){
    	$data = carro::where('iden_user',Auth::user()->id)
    				  ->select('*', 'carros.id as idcar')
    				  ->where('iden_estado','=',0)
    				  ->join('articulos','carros.iden_articulo','=','articulos.id')
    				  ->get();
    	return $data;
    }	

    public static function GetCarroAdmin($id){
       $data = carro::where('iden_user',$id)
                      ->select('*', 'carros.id as idcar')
                      ->where('iden_estado','=',0)
                      ->join('articulos','carros.iden_articulo','=','articulos.id')
                      ->get();
        return $data; 
    }

    public static function GroupCarros(){
        $data = carro::where('mont_cantidad','>',0)
                        ->join('users','carros.iden_user','=','users.id')
                        ->groupby('iden_user')
                        ->get();

        return $data;
    }
}
