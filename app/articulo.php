<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class articulo extends Model
{
    //
    public static function newArticulo($arr){
    		$articulo = new articulo;
			$articulo->desc_articulo 		= $arr[2];
            $articulo->iden_marca 			= 1;
            $articulo->iden_modelo 			= $arr[1];
            $articulo->iden_procedencia 	= 1;
            $articulo->iden_fabricante 		= 1;
            $articulo->iden_oem 			= 1;
            $articulo->nmro_stock 			=  $arr[9];
            $articulo->cant_costo 			= 10;
            $articulo->iden_oferta 			= 0;
            $articulo->fech_anioi 			= 2019;
            $articulo->fech_aniot 			= 2019;
            $articulo->desc_umedida 		=  $arr[3];
            $articulo->codi_articulo 		=  $arr[6];
            $articulo->iden_familia 		= 1;
            $articulo->estado 				= 0;
            $articulo->marca 				=  $arr[0];
            $articulo->modelo 				=  $arr[1];
            $articulo->procedencia 			=  $arr[4]; 
            $articulo->categoria 			=  $arr[8]; 
            $articulo->subcategoria 		= 'Armadura';
            $articulo->fabricante 			=  $arr[5]; 
            $articulo->busqueda 			=  $arr[2]." ".$arr[0]." ".$arr[1]." ".$arr[8]." ".$arr[6]; 
            $articulo->save();  
    }
}
