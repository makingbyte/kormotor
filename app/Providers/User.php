<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;     

    public function roles(){
        return $this->belongsToMany('App\Role');
    }
    
    public function authorizeRoles($roles){
        if($this->hasAnyRoles($roles)){
            return true;
        }else{
            abort(401, 'This action is unauthorize');
        }
    }
    
    public function checkRoles($roles){
        if($this->hasAnyRoles($roles)){
            return true;
        }else{
            return false;
        }
    }

    public function hasAnyRoles($roles){
        if(is_array($roles)){
            foreach($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }
        return false;
    }

    public function hasRole($role){

        if($this->roles()->where('name',$role)->first()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
