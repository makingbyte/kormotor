<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\oferta;
use App\articulo;

class ofertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $data         = oferta::all();
       
       /* $ofertaFiltro = articulo::join("marcas","articulos.iden_marca","=","marcas.id")
        ->select( DB::raw('count(articulos.iden_marca) as cuantos, marcas.desc_marca, articulos.id'))
        ->where('iden_oferta','>',0)
        ->groupBy('articulos.id')
        ->get();*/

        $ofertaFiltro = articulo::select( DB::raw('count(articulos.marca) as cuantos, articulos.id, articulos.marca'))
        ->where('iden_oferta','>',0)
        ->groupBy('articulos.marca')
        ->get();

       // print_r($ofertaFiltro);

        $articulos = articulo::all();
        
        return view('ofertas.ofertas', compact('data', 'articulos', 'ofertaFiltro'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ofertas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/uploads/images/', $name);
        }
        
        $id = Auth::user()->id;
        $oferta = new oferta();
        $oferta->desc_oferta = $request->input('name');
        $oferta->flag_estado = $request->input('estado');
        $oferta->desc_imagen = $name;
        $str = strtolower($request->input('name'));
        $oferta->iden_usuario = $id;
        $oferta->save();

        return redirect()->route('ofertas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data= articulo::join("ofertas","articulos.iden_oferta","=","ofertas.iden_oferta")
        //->join("marcas","articulos.iden_marca","=","marcas.iden_marca")
        ->where('articulos.iden_oferta','=',$id)
        ->get(); 

        $ofertaFiltro = articulo:://join("marcas","articulos.iden_marca","=","marcas.iden_marca")
         select( DB::raw('count(articulos.marca) as cuantos, articulos.marca, articulos.iden_marca, articulos.iden_oferta'))
        ->where('iden_oferta','=',$id)
        ->groupBy('articulos.marca')
        ->get();        

        return view('ofertas.show', compact('data','ofertaFiltro'));
    }

    public function list($id)
    {
        //
        $data = articulo::where('marca','=',$id)->get();

        ////print_r($data);
        //busco los articulos con las ofertas
        $var = explode('-',$id);
        $articuloOferta= articulo::join("ofertas","articulos.iden_oferta","=","ofertas.iden_oferta")
        //->join("marcas","articulos.iden_marca","=","marcas.iden_marca")
        ->where('articulos.iden_oferta','=',$var[0])
        ->where('articulos.marca','=',$var[1])
        ->get(); 

       // print_r($articuloOferta);

        $ofertaFiltro = articulo:://join("marcas","articulos.iden_marca","=","marcas.iden_marca")
         select( DB::raw('count(articulos.marca) as cuantos, articulos.marca, articulos.iden_marca, articulos.iden_oferta'))
        ->where('iden_oferta','=',$var[0])
        ->groupBy('articulos.marca')
        ->get();          

        return view('ofertas.list', compact('articuloOferta','ofertaFiltro'));
    }

    public function listmarca($id)
    {
        //
        $data = articulo::where('marca','=',$id)->get();

        //busco los articulos con las ofertas
        $articuloOferta= articulo::join("ofertas","articulos.iden_oferta","=","ofertas.iden_oferta")
        ->where('articulos.marca','=',$id)
        ->get(); 

        $ofertaFiltro = articulo::select( DB::raw('count(articulos.marca) as cuantos, articulos.id, articulos.marca'))
        ->where('iden_oferta','>',0)
        ->groupBy('articulos.marca')
        ->get();         

        return view('ofertas.listmarca', compact('articuloOferta','ofertaFiltro'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data  = oferta::where('iden_oferta',$id)->first();
        //var_dump($data);
        return view('ofertas.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = oferta::where('iden_oferta',$id)->first();
        $data->desc_oferta = $request->input('name');
        $data->flag_estado = $request->input('estado');
        $data->save();
        return redirect()->route('marca.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lista(){
        $data = oferta::all()->where('flag_estado',1);
        return view('ofertas.listAdmin', compact('data'));
    }
}
