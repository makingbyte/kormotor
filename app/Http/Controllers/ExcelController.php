<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\marcas;

class ExcelController extends Controller
{
    //
    public function exportUsers(){
    	
        return Excel::download(new marcaController, 'marcas.xlsx');
    }

}
