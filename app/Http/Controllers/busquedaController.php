<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\articulo;

class busquedaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $varb = explode(' ', $request->input('busq'));
       // print_r($varb);
        $num = 0;
        $coma = '';
        $from = '';

        for($i=0;$i<count($varb);$i++){
           //->where('desc_articulo','like','%'. substr($varb[$i], 0, ceil(strlen($varb[$i])/2)).'%')
            if(strlen($varb[$i])>4){
                $par = ceil(strlen($varb[$i])/2);
            }else{
                $par = strlen($varb[$i]);
            }

            if($par > 2){
                if($num>0){
                    $coma = ' AND ';
                }
                //echo 'desc_articulo like %'. substr($varb[$i], 0, $par).'%';
                $from .= $coma.' busqueda like "%'. substr($varb[$i], 0, $par).'%"';
                //$consulta .= ['desc_articulo','like','%'. substr($varb[$i], 0, $par.'%'];
                $num++;
            }
        }

        //BUSCAMOS EN LA BASE DE DATOS DE ARTICULOS
       /* $data = articulo:://where('desc_articulo','like','%'. $request->input('busq').'%')
        select('*', 'articulos.id as idart')
        ->wherein([$consulta])
        //->join("ofertas","articulos.iden_oferta","=","ofertas.iden_oferta")
        //->orWhere('iden_modelo','like','%'. $request->input('busq').'%')
        //->join('procedencias as p','articulos.iden_procedencia','=','p.id')
        //->where('desc_articulo','like','%'. substr($varb[$i], 0, ceil(strlen($varb[$i])/2)).'%')
        ->get();*/

        $data = DB::select('select *, articulos.id as idart from articulos left join ofertas on articulos.iden_oferta = ofertas.iden_oferta where '.$from);

        //echo 'select *, articulos.id as idart from articulos where '.$from;

        return view('busqueda.busqueda', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**busq
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //BUSCAMOS EN LA BASE DE DATOS DE ARTICULOS
        $data = articulo::where('desc_articulo','like','%'. $id.'%')
        ->select('*', 'articulos.id as idart')
        //->join("ofertas","articulos.iden_oferta","=","ofertas.iden_oferta")
        ->orWhere('iden_modelo','like','%'. $id.'%')
        ->orWhere('categoria','like','%'. $id.'%')
        //->join('procedencias as p','articulos.iden_procedencia','=','p.id')
        ->get();

        return view('busqueda.busqueda', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function modelo(){

        $data = articulo::all();
        $dmodelo[] = [];
        $detalle[] = '';
       // $dd[] = '';
        $i = 0;

        foreach (json_decode($data) as $key) {
           // echo $key->iden_modelo;
            foreach (explode('|', $key->iden_modelo ) as $mkey) {
                //echo $mkey;

                    //$detalle = array($mkey => '');
                if(!empty($mkey)){
                    
                    @$detalle[$mkey] += 1;
                }
                
             }
        }
         @$dd .= '[';
        foreach ($detalle as $key => $value) {
            if(!empty($value)){
                if($i > 0){ @$dd .= ',';}
                @$dd .= '{"value": "'.$value.'", "label": "'.$key.'"}';
                $i++;
            }
        }
        @$dd .= ']';

        $modelos = $dd;

        return view('busqueda.modelo', compact('data','dd'));
    }
    public function linea(){

        $data = articulo::
         select( DB::raw('count(articulos.categoria) as cuantos, articulos.categoria, articulos.iden_marca'))
        ->groupBy('articulos.categoria')
        ->get();   


       
        return view('busqueda.linea', compact('data'));
    }
}
